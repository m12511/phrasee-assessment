# resource "aws_key_pair" "phr-ec2-1-key" {
#   key_name   = "terraform-demo"
#   public_key = "${file("terraform-demo.pub")}"
# }

data "external" "my-ip-addr" {
program = ["bash", "-c", "curl -s 'https://api.ipify.org?format=json'"]
}

resource "aws_security_group" "allow-http" {
  name        = "allow-http"
  description = "Allow HTTP inbound traffic"
  vpc_id      = aws_vpc.phr-vpc.id

  ingress {
    description      = "HTTP allow"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
	#cidr_blocks      = ["${data.external.myipaddr.result.ip}/32"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  # Allow awcli access out to AWS API
  egress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  # Allow out to private subnet
  egress {
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["10.0.0.0/16"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow-http"
  }
}

resource "aws_security_group" "allow-from-proxy" {
  name        = "allow-from-proxy"
  description = "Allow from Proxy"
  vpc_id      = aws_vpc.phr-vpc.id

  ingress {
    description      = "8080 allow"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
	#cidr_blocks      = ["${data.external.myipaddr.result.ip}/32"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  # Allow awcli access out to API
  egress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow-from-proxy"
  }
}



resource "aws_instance" "phr-ec2-inst-1" {
	ami = "ami-0fdf70ed5c34c5f52"
	instance_type = "t2.nano"
  subnet_id = aws_subnet.private-subnet-1.id
  associate_public_ip_address = true
	vpc_security_group_ids = [aws_security_group.allow-from-proxy.id]
	user_data = "${file("scripts/ec2_install.sh")}"
  private_ip = "10.0.1.50"
  iam_instance_profile = "${aws_iam_instance_profile.phr-ec2-inst-1-iam-profile.name}"
  # key_name = "pha-inst-ssh"
  depends_on = [aws_route.nat-gw-route]

	tags = {
		Name = "phr-ec2-inst-1"	
	}
}


resource "aws_instance" "phr-ec2-inst-2-proxy" {
	ami = "ami-0fdf70ed5c34c5f52"
	instance_type = "t2.nano"
  subnet_id = aws_subnet.public-subnet-1.id
  associate_public_ip_address = true
	vpc_security_group_ids = [aws_security_group.allow-http.id]
	user_data = "${file("scripts/ec2_install_proxy.sh")}"
  iam_instance_profile = "${aws_iam_instance_profile.phr-ec2-inst-1-iam-profile.name}"
  # key_name = "pha-inst-ssh"
	tags = {
		Name = "phr-ec2-inst-2-proxy"	
	}
}