resource "aws_cloudwatch_dashboard" "phr-cw-nginx" {
  dashboard_name = "nginx-Logs"

  dashboard_body = <<EOF
{
    "widgets": [
        {
            "type": "log",
            "x": 0,
            "y": 0,
            "width": 24,
            "height": 6,
            "properties": {
                "query": "SOURCE '/ec2/nginx/logs' | fields @timestamp, @message\n| sort @timestamp desc\n| limit 20",
                "region": "eu-west-2",
                "stacked": false,
                "view": "table"
            }
        }
    ]
}
EOF
}