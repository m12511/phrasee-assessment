# Phrasee DevOps Offline Test

Here is a brief summary outlining the resources which are created with these terraform files, along with scripts and config files used to complete the assignment.

To keep the project portable, a local state file has been used instead of remote.

Placeholder text should not be neccessary, and should (hopefully) deploy out the box. Variables (e.g. CIDR blocks) can be edited in variables.tf / vpc.tfvars, if different IP ranges are required.

To execute these scripts use:

```
terraform init
terraform apply
```
Once finished, terraform will return the output:
```
phr-ec2-inst-2-proxy="x.x.x.x"
```
This IP address can be put into the browser to view index.html

**Note: The proxy will temporarily return a 502 error whilst the second instance finishes setting up. Please wait ~3 minutes and refresh the page**


## s3.tf
Creates a bucket (${var.bucket}) along with encrypted s3 objects. 
A bucket policy is assigned to allow access to the bucket's content via the EC2's IAM role

## ec2.tf
Two instances are created one on the public subnet and one on the private subnet.
The first instance (private) pulls the index.html file from s3 and runs an nginx container to host it.
The second instance (public) runs an nginx service configured as a reverse proxy to forward requests onto the first instance.

## vpc
Several network resources are created to host the EC2 instances:
- VPC using CIDR block 10.0.0.0/16
- A public subnet using CIDR block 10.0.0.0/24
- A private subnet using CIDR block 10.0.1.0/24
- An internet gateway to allow incomming connections to connect to the public EC2
- A NAT to allow outgoing internet connections from the private EC2

## iam.tf
An IAM role is created for the EC2 instance to grant access to s3, KMS and Cloudwatch

## scripts/ec2_install.sh / docker_run.sh
A number of packages are installed via the EC2's user_data. Including awscli, docker and awslog agent. Using awscli, index.html is copied from s3 on to the instance.
A second script is run to start a nginx containter, exposing ports and mounting the index.html file
awslogs agent is configured to stream logs from nginx access.log & error.log into Cloudwatch

## cloudwatch.tf
A cloudwatch dashboard is created to display the access.log and error.log from nginx

## scripts/nginx-reverse-proxy.conf
The public ec2 instance runs an nginx server which proxies incoming HTTP requests to a private instance listening on port 8080

