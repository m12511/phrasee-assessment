resource "aws_iam_role" "phr-ec2-inst-1-iam-role" {
  name = "phr-ec2-inst-1-iam-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
      Name = "phr-ec2-inst-1-iam-role"
  }
}

resource "aws_iam_instance_profile" "phr-ec2-inst-1-iam-profile" {
  name = "phr-ec2-inst-1-iam-profile"
  role = "${aws_iam_role.phr-ec2-inst-1-iam-role.name}"
}

resource "aws_iam_role_policy" "phr-ec2-inst-1-iam-pol" {
  name = "s3_access_pol"
  role = "${aws_iam_role.phr-ec2-inst-1-iam-role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetObject"
      ],
      "Effect": "Allow",
      "Resource": [
          "arn:aws:s3:::${var.bucket}",
          "arn:aws:s3:::${var.bucket}/*"
      ]
    },
    {
    "Sid": "VisualEditor0",
    "Effect": "Allow",
    "Action": "kms:Decrypt",
    "Resource": "${aws_kms_key.phr-asmt-kms-1.arn}"
    },
    {
        "Effect": "Allow",
        "Action": [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents",
            "logs:DescribeLogStreams"
        ],
        "Resource": [
            "arn:aws:logs:*:*:*"
        ]
    }
    
  ]
}
EOF
}
