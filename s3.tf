resource "aws_kms_key" "phr-asmt-kms-1" {
  description             = "KMS key 1"
  deletion_window_in_days = 7
}

resource "aws_s3_bucket" "phr-s3-bucket-1" {
  bucket = "${var.bucket}"
  acl    = "private"

  tags = {
    Name = "${var.bucket}"
  }
}

resource "aws_s3_bucket_object" "phr-index-html" {
  bucket = aws_s3_bucket.phr-s3-bucket-1.id
  key    = "index.html"
  source = "assets/index.html"
#   etag = filemd5("index.html")
  kms_key_id = aws_kms_key.phr-asmt-kms-1.arn
}

resource "aws_s3_bucket_object" "phr-docker-script" {
  bucket = aws_s3_bucket.phr-s3-bucket-1.id
  key    = "docker_run.sh"
  source = "scripts/docker_run.sh"
  kms_key_id = aws_kms_key.phr-asmt-kms-1.arn
}

resource "aws_s3_bucket_object" "phr-nginx-pp-conf" {
  bucket = aws_s3_bucket.phr-s3-bucket-1.id
  key    = "nginx-reverse-proxy.conf"
  source = "scripts/nginx-reverse-proxy.conf"
  kms_key_id = aws_kms_key.phr-asmt-kms-1.arn
}

resource "aws_s3_bucket_object" "phr-aws-logs-conf" {
  bucket = aws_s3_bucket.phr-s3-bucket-1.id
  key    = "awslogs.conf"
  source = "scripts/awslogs.conf"
  kms_key_id = aws_kms_key.phr-asmt-kms-1.arn
}

resource "aws_s3_bucket_policy" "phr-s3-bucket-pol" {
  bucket = aws_s3_bucket.phr-s3-bucket-1.id
  policy = jsonencode({
    "Version": "2012-10-17",
    "Id": "Policy",
    "Statement": [
        {
            "Sid": "statement",
            "Effect": "Allow",
            "Principal": {
                "AWS": "${aws_iam_role.phr-ec2-inst-1-iam-role.arn}"
            },
            "Action": "s3:*",
            "Resource": [
                "arn:aws:s3:::${var.bucket}/*",
                "arn:aws:s3:::${var.bucket}"
            ]
        }
    ]
})
}
