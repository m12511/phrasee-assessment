#!/bin/bash
echo "Starting" > /tmp/init.log
apt -y update
apt -y install nginx awscli
ufw allow 'Nginx HTTP'


unlink /etc/nginx/sites-enabled/default
aws s3 cp s3://phr-html/nginx-reverse-proxy.conf /etc/nginx/sites-enabled/default
service nginx restart