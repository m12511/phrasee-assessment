#!/bin/bash
echo "Starting" > /tmp/init.log
apt -y update
apt -y install nginx awscli
ufw allow 'Nginx HTTP'

#
# Copy index.html from s3
#

aws s3 cp s3://phr-html/index.html /var/www/html/index.html

#
# Install docker / nginx
#

apt -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
apt-cache policy docker-ce
apt -y install docker-ce

usermod -aG docker ${USER}

# docker run -p8080:80 nginx

aws s3 cp s3://phr-html/docker_run.sh /tmp/docker_run.sh

bash /tmp/docker_run.sh


#
# AWS Cloudwatch agent
#

apt -y install python
aws s3 cp s3://phr-html/awslogs.conf /tmp/awslogs.conf
curl https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py -O
python awslogs-agent-setup.py -n -r eu-west-2 -c /tmp/awslogs.conf
service awslogs restart

echo "Done" >> /tmp/init.log
echo "READY!" >> /var/log/nginx/access.log