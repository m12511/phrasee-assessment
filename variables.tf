variable "region" {
    default = "eu-west-2"
    description = "AWS region"
}

variable "bucket" {
    default = "phr-html"
    description = "HTML bucket"
}

variable "vpc_cidr" {
    default = "10.0.0.0/16"
    description = "CIDR Block for VPC"
}

variable "public_subnet_1_cidr" {
    default = "10.0.0.0/24"
    description = "CIDR Block for Public Subnet 1"
}

variable "private_subnet_1_cidr" {
    default = "10.0.1.0/24"
    description = "CIDR Block for Private Subnet 1"
}
