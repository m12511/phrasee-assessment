provider "aws" {
    region = "${var.region}"
}

# terraform {
#   backend "s3" {
#     bucket = "phr-tfstate-store"
#     key    = "assessment.tfstate"
#     region = "eu-west-2"
#   }
# }